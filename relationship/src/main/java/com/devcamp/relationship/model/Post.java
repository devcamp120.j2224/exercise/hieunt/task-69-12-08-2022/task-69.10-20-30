package com.devcamp.relationship.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "posts")
public class Post {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
	
	@Column(name = "title")
    private String title;
	
	@Column(name = "content")
    private String content;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by", nullable = false)
    private User createdBy;

	@ManyToMany(fetch = FetchType.LAZY,
		cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "post_tags",
		joinColumns = {@JoinColumn(name = "post_id")},
		inverseJoinColumns = {@JoinColumn(name = "tag_id")})
    private Set<HashTag> tags;
	
	public Post() {
		super();
	}

	public Post(long id, String title, String content, User createdBy, Set<HashTag> tags) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.createdBy = createdBy;
		this.tags = tags;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Set<HashTag> getTags() {
		return tags;
	}

	public void setTags(Set<HashTag> tags) {
		this.tags = tags;
	}
}
